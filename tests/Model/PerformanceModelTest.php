<?php
namespace App\Tests\Model;

use App\Model\DataInputModel;
use App\Model\PerformanceModel;
use PHPUnit\Framework\TestCase;

class PerformanceModelTest extends TestCase
{
    private $objectToTest;

    public function setUp()
    {
        $data = [
            new DataInputModel(
                3,
                "2018-01-29"
            ),
            new DataInputModel(
                13,
                "2018-01-30"
            ),
            new DataInputModel(
                7,
                "2018-01-31"
            ),
            new DataInputModel(
                5,
                "2018-02-01"
            ),
            new DataInputModel(
                21,
                "2018-02-02"
            ),
            new DataInputModel(
                23,
                "2018-02-03"
            ),
            new DataInputModel(
                23,
                "2018-02-04"
            ),
            new DataInputModel(
                40,
                "2018-02-05"
            ),
            new DataInputModel(
                23,
                "2018-02-06"
            ),
            new DataInputModel(
                14,
                "2018-02-07"
            ),
            new DataInputModel(
                12,
                "2018-02-08"
            ),
            new DataInputModel(
                56,
                "2018-02-09"
            ),
            new DataInputModel(
                23,
                "2018-02-10"
            ),
            new DataInputModel(
                29,
                "2018-02-11"
            )
        ];
        $this->objectToTest = new PerformanceModel(
            $data,
            "b"
        );
    }

    public function testPeriodPerformance()
    {
        $this::assertEquals(
            [
                '2018-01-29',
                '2018-02-11'
            ],
            $this->objectToTest->getPeriodPerformance()
        );
    }

    public function testStadistictsPerformance()
    {
        $this::assertEquals(
            [
                'b',
                20.85,
                3.0,
                56.0,
                22.0
            ],
            $this->objectToTest->getStadistictsPerformance()
        );
    }

    public function testGetDatesUnderPerforming()
    {
        $this::assertIsArray(
            $this->objectToTest->getDatesUnderPerforming()
        );

        $this::assertEquals(
            [
                "2018-01-29", "2018-01-30", "2018-01-31", "2018-02-01", "2018-02-02", "2018-02-07", "2018-02-08"
            ],
            $this->objectToTest->getDatesUnderPerforming()
        );
    }
}