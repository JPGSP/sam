<?php
namespace App\Tests\Model;

use App\Model\PeriodModel;
use PHPUnit\Framework\TestCase;

class PeriodModelTest extends TestCase
{
    private $objectToTest;

    public function setUp()
    {
        $this->objectToTest = new PeriodModel(
            [
                "2010-01-29",
                "2009-01-30",
                "1908-01-31",
                "2000-02-01"
            ]
        );
    }

    public function testEarliestDate()
    {
        $this::assertEquals(
            "1908-01-31",
            $this->objectToTest->getEarliest()
        );
    }

    public function testLastestDate()
    {
        $this::assertEquals(
            "2010-01-29",
            $this->objectToTest->getLatest()
        );
    }

    public function testDateInPosition()
    {
        $this::assertEquals(
            "2009-01-30",
            $this->objectToTest->getByKey(1)
        );
    }
}