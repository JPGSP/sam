<?php
namespace App\Tests\Model;

use App\Model\StadisticModel;
use PHPUnit\Framework\TestCase;

class StadisticModelTest extends TestCase
{
    private $objectToTest;

    public function setUp()
    {
        $this->objectToTest = new StadisticModel(
            [12648239.57, 12693166.98, 12650003.275, 12600003.275],
            "b"
        );
    }

    public function testIsUnitValid()
    {
        $this::assertEquals(
            "b",
            $this->objectToTest->getUnit()
        );
    }

    public function testMaximumValue()
    {
        $this::assertEquals(
            12693166.98,
            $this->objectToTest->getMaximum()
        );
    }

    public function testMinimumValue()
    {
        $this::assertEquals(
            12600003.275,
            $this->objectToTest->getMinimum()
        );
    }

    public function testAverageValue()
    {
        $this::assertEquals(
            12647853.275,
            $this->objectToTest->getAverage()
        );
    }

    public function testMedianValue()
    {
        $this::assertEquals(
            12649121.4225,
            $this->objectToTest->getMedian()
        );
    }

    public function testGetPositionsWhereMetricSmallerThanMedian()
    {
        $this::assertEquals(
            [0,3],
            $this->objectToTest->getPositionSmallerThanMedian()
        );
    }
}