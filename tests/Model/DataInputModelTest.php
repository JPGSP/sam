<?php
namespace App\Tests\Model;

use App\Model\DataInputModel;
use PHPUnit\Framework\TestCase;

class DataInputModelTest extends TestCase
{
    private $objectToTest;

    protected function setUp()
    {
        $this->objectToTest = new DataInputModel(
            12693166.98,
            "2018-01-29"
        );
    }

    public function testMetricValue()
    {
        $this::assertEquals(
            12693166.98,
            $this->objectToTest->getMetric()
        );
    }

    public function testDateValue()
    {
        $this::assertEquals(
            "2018-01-29",
            $this->objectToTest->getDate()
        );
    }
}