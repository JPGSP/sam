<?php
namespace App\Tests\Service;

use App\Model\DataInputModel;
use App\Service\Reader;
use PHPUnit\Framework\TestCase;

class ReaderTest extends TestCase
{
    public function testReadFile()
    {
        $content = Reader::getContentFile(
            'tests/Resources/inputs/1.json'
        );

        $this::assertIsArray($content);
        $this::assertCount(2, $content);
        $this::assertEquals(
            new DataInputModel(
                12693166.98,
                "2018-07-31"
            ),
            $content[0]
        );
    }
}