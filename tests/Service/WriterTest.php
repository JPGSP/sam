<?php
namespace App\Tests\Service;

use App\Model\DataInputModel;
use App\Service\Writer;
use PHPUnit\Framework\TestCase;

class WriterTest extends TestCase
{
    private $objectToTest;

    public function setUp()
    {
        $data = [
            new DataInputModel(
                12693166.98, // 1.5866458725
                "2018-01-29"
            ),
            new DataInputModel(
                12668239.57, //1.58352994625
                "2018-01-30"
            ),
            new DataInputModel(
                12723772.1,  //1.5904715125
                "2018-01-31"
            ),
            new DataInputModel(
                12555239.57,
                "2018-02-01"
            ),
            new DataInputModel(
                12555209.57,
                "2018-02-02"
            ),
            new DataInputModel(
                12545209.57,
                "2018-02-03"
            ),
            new DataInputModel(
                12645209.57,
                "2018-02-04"
            )
        ];
        $this->objectToTest = new Writer($data, "Megabytes");
    }

    public function testContentPeriodChecked()
    {
        $this::assertEquals(
            "From: 2018-01-29\nTo: 2018-02-04",
            $this->objectToTest->getContentPeriod()
        );
    }

    public function  testContentStadistics()
    {
        $this::assertIsArray($this->objectToTest->getContentStadistics());

        $this::assertCount(5, $this->objectToTest->getContentStadistics());

        $this::assertEquals(
            "Units: Megabytes per second",
            $this->objectToTest->getContentStadistics()[0]
        );
        $this::assertEquals(
            "Average: 1.57",
            $this->objectToTest->getContentStadistics()[1]
        );
        $this::assertEquals(
            "Min: 1.56",
            $this->objectToTest->getContentStadistics()[2]
        );
        $this::assertEquals(
            "Max: 1.59",
            $this->objectToTest->getContentStadistics()[3]
        );
        $this::assertEquals(
            "Median: 1.58",
            $this->objectToTest->getContentStadistics()[4]
        );
    }

    public function testContentUnderPerforming()
    {
        $this::assertEquals(
            "2018-02-01 and 2018-02-03",
            $this->objectToTest->getContentUnderPerforming()[0]
        );
    }
}