<?php
namespace App\Tests\Service;

use App\Service\Conversor;
use PHPUnit\Framework\TestCase;

class ConversorTest extends TestCase
{
    private $data;

    protected function setUp()
    {
        $this->data = [
            12693166.98,
            12668239.57
        ];
    }

    public function testConversorUnit()
    {
        $this::assertEquals(
            '12693166.98',
            Conversor::getValueIn($this->data[0], "bits")
        );

        $this::assertEquals(
            '1586645.8725',
            Conversor::getValueIn($this->data[0], "bytes")
        );

        $this::assertEquals(
            '1.58352994625',
            Conversor::getValueIn($this->data[1], "MB")
        );
    }
}