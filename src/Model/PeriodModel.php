<?php
namespace App\Model;

class PeriodModel
{
    private $dateSet;

    public function __construct(array $dateInputSet)
    {
        $this->dateSet = $dateInputSet;
    }

    public function getEarliest(): string
    {
        return min($this->dateSet);
    }

    public function getLatest(): string
    {
        return max($this->dateSet);
    }

    public function getByKey(int $parameter): string
    {
        return $this->dateSet[$parameter];
    }
}