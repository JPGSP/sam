<?php


namespace App\Model;

use App\Service\Conversor;

class StadisticModel
{
    private $unit;
    private $metricSet;

    public function __construct(array $metricValueSet, string $unit)
    {
        $this->unit = $unit;
        foreach ($metricValueSet as $value) {
            $this->metricSet[] = Conversor::getValueIn($value, $unit);
        }
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function getMaximum(): float
    {
        return max($this->metricSet);
    }

    public function getMinimum(): float
    {
        return min($this->metricSet);
    }

    public function getAverage(): float
    {
        return array_sum($this->metricSet) / count($this->metricSet);
    }

    public function getMedian(): float
    {
        $data = $this->metricSet;

        sort($data);
        $count = count($data);
        $middleval = floor(($count - 1)/2);

        $median = ($count % 2) ?
            $data[$middleval] :
            (($data[$middleval] + $data[$middleval+1]) / 2);

        return $median;
    }

    public function getPositionSmallerThanMedian(): array
    {
        $outputData = [];

        foreach ($this->metricSet as $key => $value) {
            if ($value < $this->getMedian()) {
                $outputData[] = $key;
            }
        }

        return $outputData;
    }
}