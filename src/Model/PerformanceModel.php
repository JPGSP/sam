<?php
namespace App\Model;


class PerformanceModel
{
    private $dateSet;
    private $metricSet;

    public function __construct(array $dataInputSet, string $unit)
    {
        $dates = [];
        $metrics = [];

        foreach ($dataInputSet as $dataInput) {
            $dates[] = $dataInput->getDate();
            $metrics[] = $dataInput->getMetric();
        }

        $this->dateSet = new PeriodModel($dates);
        $this->metricSet = new StadisticModel($metrics, $unit);
    }

    public function getPeriodPerformance(): array
    {
        return [
            $this->dateSet->getEarliest(),
            $this->dateSet->getLatest()
        ];
    }

    public function getStadistictsPerformance(): array
    {
        return [
            $this->metricSet->getUnit(),
            floor($this->metricSet->getAverage() * 100) /100,
            floor($this->metricSet->getMinimum() * 100) /100,
            floor($this->metricSet->getMaximum() * 100) /100,
            floor($this->metricSet->getMedian() * 100) /100
        ];
    }

    public function getDatesUnderPerforming(): array
    {
        $positionsMetricUnderPerforming = $this->metricSet->getPositionSmallerThanMedian();
        $dateUnderPerforming = [];

        foreach($positionsMetricUnderPerforming as $positionUnderPerforming) {
            $dateUnderPerforming[] = $this->dateSet->getByKey($positionUnderPerforming);
        }

        return $dateUnderPerforming;
    }
}