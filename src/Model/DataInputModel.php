<?php


namespace App\Model;


class DataInputModel
{
    private $metric;
    private $date;

    public function __construct(float $metricValue, string $dateValue)
    {
        $this->metric = $metricValue;
        $this->date = $dateValue;
    }

    public function getMetric(): float
    {
        return $this->metric;
    }

    public function getDate(): string
    {
        return $this->date;
    }
}