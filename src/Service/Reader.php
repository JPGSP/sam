<?php
namespace App\Service;

use App\Model\DataInputModel;

class Reader
{
    public static function getContentFile($location): array
    {
        $json = file_get_contents($location);
        $json_data = json_decode($json, true);
        $arrayResult = [];

        foreach ($json_data as $arrayData) {
            $arrayResult[] = new DataInputModel($arrayData['metricValue'],$arrayData['dtime']);
        }

        return $arrayResult;
    }
}