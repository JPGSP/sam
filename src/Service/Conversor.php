<?php
namespace App\Service;


class Conversor
{
    public static function getValueIn(float $value, string $unit): float
    {
        $factor = 0;

        switch ($unit) {
            case "bits":
            case "bit":
            case "Bits":
            case "Bit":
            case "b":
                $factor = 1;
                break;
            case "megabits":
            case "megabit":
            case "mb":
            case "Megabits":
            case "Megabit":
                $factor = 1000000;
                break;
            case "bytes":
            case "byte":
            case "Bytes":
            case "Byte":
            case "B":
                $factor = 8;
                break;
            case "megabytes":
            case "megabyte":
            case "Megabytes":
            case "Megabyte":
            case "MB":
                $factor = 8000000;
                break;
        }

        return $value / $factor;
    }
}