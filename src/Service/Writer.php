<?php
namespace App\Service;

use App\Model\PerformanceModel;

class Writer
{
    private $data;

    public function __construct(array $data, string $unit)
    {
        $this->data = new PerformanceModel($data, $unit);
    }

    public function getContentPeriod(): string
    {
        $dateSet = $this->data->getPeriodPerformance();
        return "From: " . $dateSet[0] ."\nTo: " . $dateSet[1];
    }

    public function getContentStadistics(): array
    {
        $toPrint = $this->data->getStadistictsPerformance();
        return [
            sprintf("Units: %s per second", $toPrint[0]),
            sprintf("Average: %.2f", $toPrint[1]),
            sprintf("Min: %.2f", $toPrint[2]),
            sprintf("Max: %.2f", $toPrint[3]),
            sprintf("Median: %.2f", $toPrint[4])
        ];
    }

    public function getContentUnderPerforming(): string
    {
        $salida = $this->data->getPeriodPerformance();

        return $salida;
    }


}