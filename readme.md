# SAM Test

The purpose of this project is to process external data 
(read from a file) following SOLID principes.

## System requirements

 - PHP 7.0.0 or above.
 - Git.
 - Composer.
 
## Download the source code:

Go to the folder where you have all your projects:

```bash
cd /path/to/projects/folder/
```

Then clone the Git Repository:

```bash
git clone https://gitlab.com/JPGSP/sam.git
```

or 

```bash
git clone git@gitlab.com:JPGSP/sam.git
```

Once the previous process has finished a new folder 
```sam``` will be created.

## Usage

- Install dependecies

Go inside the folder just created:

```bash
cd /path/to/projects/folder/sam
```

Install project dependencies

```bash
composer install
```

## Next?

- Fix the part of the code responsible for taking the dates 
under performing.

- Create a command to write a report (path given as a parameter)
using the service 'Write'.

- Add more test cases.

- Add composer.

- Remove symfony components/elements not needed
(e.g. the controller folder, in case the command solution 
is valid)